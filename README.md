# RabiitMQ Publisher Subscribe - Pastrana Cristian
Ejemplo nro. 3 del  Tutorial RabbitMQ -Publisher Subscribe
https://www.rabbitmq.com/tutorials/tutorial-three-python.html

**¿Que necesitamos?**
- Docker > https://docs.docker.com/docker-for-windows/install/

**¿Como ejecuto el programa?**

- Clonamos el repositorio donde nos quede mas comodo:

- En ese mismo directorio creamos la imagen:
>docker-compose build

- Luego levantamos los contenedores:
>docker-compose up

**Recibir mensajes**

- Iniciamos una cola que recibira los mensajes:
>docker exec receive-logs python receive_logs.py

- Por consola se mostraran los mensajes recibidos, CTRL+C si queremos cerrar la aplicacion.

**Enviar mensajes**

- Para enviar un mensaje ingresamos:
>docker exec emit-logs python emit_log.py "mensaje"

- Si no ingresamos un "mensaje", se enviara un mensaje generico.

**¿Como dejo de ejecutar los contenedores?**

- Para detener los contenedores:
> docker-compose stop

- Para iniciar los contenedores:
> docker-compose start


